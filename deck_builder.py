import requests
import json
import random

headers = {
    'Content-Type': 'application/json'
}

class Commander():
    def __init__(self,name, colors,cmc,abilities):
        self.commander_name = name
        self.commander_colors = colors
        self.commander_cmc = cmc
        self.commander_abilities = abilities

    def __str__(self):
        return f'Your chosen commander is {self.commander_name}. Its colors are {self.commander_colors} with a Converted Mana Cost of {self.commander_cmc}'

class Card():
    def __init__(self,name,c_type,color,cmc,abilities):
        self.card_name = name
        self.card_c_type = c_type
        self.card_color = color
        self.card_cmc = cmc
        self.card_abilities = abilities

    def __str__(self):
        return f'The card name is {self.card_name}, the type is {self.card_c_type}, its Converted Mana Cost is {self.card_cmc}, and has these abilities {self.card_abilities}'

# Gets the land types
def get_land_types():
    response = requests.get('https://api.scryfall.com/catalog/land-types', headers=headers)
    land_content = response.content
    land_data = open('land_type.json', 'w')
    land_data.write(str(land_content))
    land_data.close()

    land_types = json.loads(land_content)
    print(json.dumps(land_types, indent=4, sort_keys=True))


get_land_types()